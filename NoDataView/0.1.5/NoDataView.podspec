#
# Be sure to run `pod lib lint NoDataView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NoDataView'
  s.version          = '0.1.5'
  s.summary          = 'A short description of NoDataView.'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/tool11/NoDataView.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'wangguimin' => '870503894@qq.com' }
  s.source           = { :git => 'https://gitlab.com/tool11/NoDataView.git', :tag => 'master' }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'NoDataView/Classes/**/*'
  
  # s.resource_bundles = {
  #   'NoDataView' => ['NoDataView/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
    s.dependency 'AFNetworking'
    s.dependency 'MJRefresh', '~> 3.4.3'
end
